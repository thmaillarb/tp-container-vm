[[TP1_conteneur_vm.pdf|Sujet]]

## Partie 1 - VirtualBox

### Question 1

On installe le package `net-tools` (`sudo apt install net-tools`) et on utilise la commande `ifconfig` :

![](images/Pasted%20image%2020240118150037.png)

On sait que l'interface `lo` correspond au loopback, donc on voit que l'IP privée est `10.0.2.15`.

### Question 2

On utilise la commande `ping` avec le paramètre `-n` à 1, ce qui nous permet de limiter le nombre de tentatives à 1. On obtient le résultat suivant :

![](images/Pasted%20image%2020240118150411.png)

On remarque qu'il a été impossible de ping la machine virtuelle. C'est un comportement attendu puisque nous utilisons une carte réseau NAT et que nous n'avons pas configuré de redirection de port.

### Question 3

On ajoute une carte réseau de type Host only :
![](images/Pasted%20image%2020240118150655.png)

On redémarre la VM et on relance la commande `ifconfig` : 

![](images/Pasted%20image%2020240118150855.png)

On remarque une nouvelle interface réseau nommée `enp0s8` et qui a l'adresse IP `192.168.56.101`. On relance la commande ping avec les mêmes paramètres et la nouvelle adresse IP :

![](images/Pasted%20image%2020240118151017.png)

Le paquet a été reçu, on peut donc se connecter depuis la machine hôte à notre VM en passant par cette carte réseau.

### Question 4

Nous utilisons la commande `ssh-keygen`. Pour des raisons de sécurité, nous utilisons un chiffrement RSA sur 4096 bits :

![](images/Pasted%20image%2020240118151342.png)

### Question 5

Nous allons nous connecter en tant que `root` et ajouter notre clé privée :

![](images/Pasted%20image%2020240125134923.png)

On installe `openssh-server` avec la commande suivante :

```bash
sudo apt install -y openssh-server
```

On crée ou on modifie un fichier `/etc/ssh/sshd_config` afin de vérifier que les paramètres suivants soient correctement réglés. On veut autoriser les connections à `root`, et n'autoriser que les connections par clé :

```
PermitRootLogin without-password
PasswordAuthentication no
PubkeyAuthentication yes
```

On redémarre le daemon ssh avec la commande suivante :

```bash
sudo systemctl restart sshd
```

Le daemon est bien lancé :

![](images/Pasted%20image%2020240125140007.png)

### Question 6

Depuis le système hôte, on utilise la commande suivante :

![](images/Pasted%20image%2020240125140212.png)

On remarque que nous sommes bien connectés à la machine virtuelle en ssh à `root`.

Essayons de nous connecter via mot de passe :

![](images/Pasted%20image%2020240125140407.png)

La connection est bien refusée.

## Partie 2 - Ansible

### Question 1

On crée un fichier ini (ici, `inventory.ini`) contenant ceci :

```ini
[mygroup]
192.168.56.101 ansible_ssh_user=root
```

![](images/Pasted%20image%2020240125144515.png)

### Question 2

![](images/Pasted%20image%2020240125144640.png)

### Question 3

Fichier `playbook.yaml` :

```yaml
---
- name: Create temporary file and check it exists
  hosts: mygroup

  tasks:
      - name: Check file
      stat:
        path: "/tmp/myfile"
      register: stat_result
      
    - name: Create file
      file:
        path: "/tmp/myfile"
        state: touch
      when: not stat_result.stat.exists
```

![](images/Pasted%20image%2020240125145853.png)

### Question 4

On ajoute la tâche suivante :

```yml
- name: Apache2
  hosts: mygroup

  tasks:
    - name: Install apache2
      apt: name=apache2 update_cache=yes state=latest
```

![](images/Pasted%20image%2020240125150245.png)

### Question 5

On ajoute cette tâche :

```yml
    - name: Add file
      ansible.builtin.copy:
        src: index.html
        dest: /var/www/html/index.html
        force: true
        owner: root
        group: root
        mode: '0644'
```

![](images/Pasted%20image%2020240125151630.png)

![](images/Pasted%20image%2020240125151640.png)

### Question 6

Le play devient :

```yml
- name: Apache2
  hosts: mygroup

  tasks:
    - name: Install apache2
      apt: name=apache2 update_cache=yes state=latest

    - name: Add file
      ansible.builtin.copy:
        src: index.html
        dest: /var/www/html/index.html
        force: true
        owner: root
        group: root
        mode: '0644'
      register: addfile

    - name: Restart apache2
      service: name=apache2 state=restarted
      when: addfile.changed
```

On lance sans modifier le fichier html : 

![](images/Pasted%20image%2020240125152101.png)

On lance en modifiant le fichier html :

![](images/Pasted%20image%2020240125152208.png)

### Question 7

On crée la tâche suivante :

```yaml
    - name: Get site
      ansible.builtin.command: curl localhost
      register: site
```

On modifie également la tâche "Install apache2" en "Install apache2 and curl" :

```yaml
    - name: Install apache2 and curl
      apt:
        pkg:
          - curl
          - apache2
        update_cache: yes
        state: latest
```
### Question 8

On crée le bloc de tâches suivantes :

```yaml
    - name: Check site
      block:
        - name: Site OK
          debug: var=site.stdout
          when: site.rc == 0

        - name: Site KO
          debug: msg="Site is KO"
          when: site.rc != 0
```

![](images/Pasted%20image%2020240125155809.png)
